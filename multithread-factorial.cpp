#include <thread>
#include <iostream>
#include <mutex>
#include <vector>
#include <string>
#include <Windows.h>

using namespace std;

vector<int> numbers;
mutex mute;

void calculate(int no, int length, int* nums) {
	mute.lock();

	int total = 1;

	//Sleep(500);
	for (int i = 0; i < length; i++)
		total *= nums[i];

	cout << "- Result of Thread #" << no << " is " << total << endl;
	numbers.push_back(total);

	mute.unlock();
}

int main() {
	int n;

	cout << "Input factorial number: ";
	cin >> n;

	cout << "\n=== PROCESS === \n";

	for (int i = n; i > 0; i--)
		numbers.push_back(i);

	vector<thread> threads;

	for (int i = 0; i < n - 1; i++) {
		int j = 0;
		int* nums = new int[2];
		string teks = "";

		while (j < 2) {
			if (numbers.size() > 0) {
				nums[j] = numbers.front();
				numbers.erase(numbers.begin());

				teks.append(to_string(nums[j]));
				if (j < 1)
					teks.append(" * ");

				j++;
			}
		}

		cout << "+ Creating Thread #" << i + 1 << " (" << teks << ")" << endl;

		threads.push_back(thread(calculate, i + 1, 2, nums));
	}

	for (int i = 0; i < threads.size(); i++)
		threads.at(i).join();

	cout << "\n= Result of " << n << "! is " << numbers.front() << endl << endl;

    return 0;
}